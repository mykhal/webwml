#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans dwarfutils, un outil
et une bibliothèque pour lire ou interpréter et écrire ou produire des
informations de débogage de DWARF. Le projet « Common Vulnerabilities and
Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8538">CVE-2015-8538</a>

<p>Un fichier ELF contrefait pour l'occasion peut provoquer une erreur de
segmentation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8750">CVE-2015-8750</a>

<p>Un fichier ELF contrefait pour l'occasion peut provoquer un
déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2050">CVE-2016-2050</a>

<p>Écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2091">CVE-2016-2091</a>

<p>Lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5034">CVE-2016-5034</a>

<p>Écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5036">CVE-2016-5036</a>

<p>Lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5038">CVE-2016-5038</a>

<p>Lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5039">CVE-2016-5039</a>

<p>Lecture hors limites</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5042">CVE-2016-5042</a>

<p>Une section DWARF contrefaite pour l'occasion peut provoquer une boucle
infinie, en lisant à partir d'adresses de mémoire croissantes jusqu'au
plantage de l'application.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 20120410-2+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets dwarfutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-669.data"
# $Id: $
