#use wml::debian::translation-check translation="7e6b86e512d1e203a20619cdee7230be27c2eae5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL, une boîte
à outils associée à SSL (Secure Socket Layer).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2105">CVE-2016-2105</a>

<p>Guido Vranken a découvert qu'un dépassement pouvait se produire dans la
fonction EVP_EncodeUpdate(), utilisée pour l'encodage Base64, si un
attaquant pouvait fournir une grande quantité de données. Cela pourrait
conduire à une corruption de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2106">CVE-2016-2106</a>

<p>Guido Vranken a découvert qu'un dépassement pouvait se produire dans la
fonction EVP_EncryptUpdate() si un attaquant pouvait fournir une grande
quantité de données. Cela pourrait conduire à une corruption de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2107">CVE-2016-2107</a>

<p>Juraj Somorovsky a découvert une attaque d'oracle par remplissage dans
l'implémentation du chiffrement par bloc AES CBC basé sur l'ensemble
d'instructions AES-NI. Cela pourrait permettre à un attaquant de décoder le
trafic TLS chiffré avec une des suites de chiffrement basées sur AES CBC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2108">CVE-2016-2108</a>

<p>David Benjamin de Google a découvert que deux bogues distincts dans
l'encodeur ASN.1, relatifs au traitement de valeurs d'entier zéro négatif
et de grands « universal tags », pourraient conduire à une écriture hors
limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2109">CVE-2016-2109</a>

<p>Brian Carpenter a découvert que, lors de la lecture de données ASN.1
à partir d'un BIO utilisant des fonctions telles que d2i_CMS_bio(), un
encodage court incorrect peut provoquer l'allocation d'une grande quantité
de mémoire et éventuellement une consommation excessive de ressources ou
l'épuisement de la mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2176">CVE-2016-2176</a>

<p>Guido Vranken a découvert que des chaînes ASN.1 plus longues que
1024 octets peuvent provoquer une lecture hors limites dans les
applications qui utilisent la fonction X509_NAME_oneline() sur les systèmes
EBCDIC. Cela pourrait avoir pour conséquence que des données arbitraires de
la pile soient renvoyées dans le tampon.</p></li>

</ul>

<p>Des informations supplémentaires sur ces problèmes peuvent être trouvées
dans l'annonce de sécurité d'OpenSSL à l'adresse
<a href="https://www.openssl.org/news/secadv/20160503.txt">https://www.openssl.org/news/secadv/20160503.txt</a></p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.0.1e-2+deb7u21 d'openssl.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-456.data"
# $Id: $
