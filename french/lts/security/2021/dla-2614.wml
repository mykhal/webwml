#use wml::debian::translation-check translation="7a08138c061c31448da5b1d7a6d1f01b7ceb1ca9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le décompresseur gunzip de Busybox, un ensemble d’utilitaires pour des
systèmes petits et embarqués, gère de manière incorrecte le bit d’erreur sur
le pointeur du résultat de huft_build, aboutissant à une libération non valable
ou à une erreur de segmentation, à l’aide de données gzip malformées.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1:1.22.0-19+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets busybox.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de busybox, veuillez
consulter sa page de suivi de sécurité à l'adresse:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/busybox">\
https://security-tracker.debian.org/tracker/busybox</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2614.data"
# $Id: $
