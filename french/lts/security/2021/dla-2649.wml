#use wml::debian::translation-check translation="968979b6040ac08d77d1d80dc9e4f86e35e19ea6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Quatre problèmes de sécurité ont été découverts dans cgal. Une vulnérabilité
d’exécution de code existe dans la fonction d’analyse de polygone nef de CGAL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28601">CVE-2020-28601</a>

<p>Une vulnérabilité de lecture hors limites existe dans
Nef_2/PM_io_parser.h PM_io_parser::read_vertex() Face_of[].
Un attaquant peut fournir une entrée malveillante pour déclencher cette
vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28636">CVE-2020-28636</a>

<p>Une vulnérabilité de lecture hors limites existe dans
Nef_S2/SNC_io_parser.h SNC_io_parser::read_sloop() slh->twin().
Un attaquant peut fournir une entrée malveillante pour déclencher cette
vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35628">CVE-2020-35628</a>

<p>Une vulnérabilité de lecture hors limites existe dans
Nef_S2/SNC_io_parser.h SNC_io_parser::read_sloop() slh->incident_sface.
Un attaquant peut fournir une entrée malveillante pour déclencher cette
vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35636">CVE-2020-35636</a>

<p>Une vulnérabilité de lecture hors limites existe dans
Nef_S2/SNC_io_parser.h SNC_io_parser::read_sface() sfh->volume().
Un attaquant peut fournir une entrée malveillante pour déclencher cette
vulnérabilité.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.9-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cgal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cgal, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cgal">\
https://security-tracker.debian.org/tracker/cgal</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2649.data"
# $Id: $
