#use wml::debian::translation-check translation="c12ce2637a089a0dd9673047810df021b3fb8fbe" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les CVE suivants ont été signalés à l’encontre de src:openjpeg2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12973">CVE-2019-12973</a>

<p>Dans OpenJPEG 2.3.1, il existait une itération excessive dans la fonction
opj_t1_encode_cblks de openjp2/t1.c. Des attaquants distants pouvaient exploiter
cette vulnérabilité pour provoquer un déni de service à l'aide d'un fichier bmp
contrefait. Ce problème est similaire
à <a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6851">CVE-2020-6851</a>

<p>OpenJPEG jusqu’à la version 2.3.1 possédait un dépassement de tampon de tas
dans opj_t1_clbl_decode_processor dans openjp2/t1.c à cause d’un manque de
validation de opj_j2k_update_image_dimensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8112">CVE-2020-8112</a>

<p>opj_t1_clbl_decode_processor dans openjp2/t1.c dans OpenJPEG 2.3.1
jusqu’au 28/01/2020 possédait un dépassement de tampon de tas dans le cas où
qmfbid==1, un problème différent de
<a href="https://security-tracker.debian.org/tracker/CVE-2020-6851">CVE-2020-6851</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15389">CVE-2020-15389</a>

<p>jp2/opj_decompress.c dans OpenJPEG jusqu’à la version 2.3.1 comprenait une
utilisation de mémoire après libération pouvant être déclenchée s’il existait
un mélange de fichiers valables et non valables dans un répertoire traité par le
décompresseur. Le déclenchement d’une double libération de zone de mémoire
était aussi possible. Cela concernait un double appel de
opj_image_destroy.</p></li>

</ul>

<p>Pour Debian 9 <q>stretch</q>, ces problèmes ont été corrigés dans
la version 2.1.2-1.1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openjpeg2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openjpeg2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openjpeg2">https://security-tracker.debian.org/tracker/openjpeg2</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2277.data"
# $Id: $
