#use wml::debian::translation-check translation="442f469fa9c328b419c71979b178aa6a2ece01fc" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dans httplib2, un attaquant, contrôlant une partie non protégée d’un URI pour
« httplib2.Http.request() », pourrait modifier les en-têtes et le corps d’une
requête et envoyer des requêtes cachées supplémentaires au même serveur. Cette
vulnérabilité impacte les logiciels utilisant httplib2 avec un URI construit par
concaténation de chaînes, contrairement à une construction propre d’urllib
avec protection.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 0.9+dfsg-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-httplib2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2232.data"
# $Id: $
