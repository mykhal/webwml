#use wml::debian::translation-check translation="4f3091355e142513acf946fa22017cba5fca0ba8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes de sécurité ont été identifiés et corrigés dans php5, un
langage de script embarqué dans du HTML et côté serveur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7062">CVE-2020-7062</a>

<p>Déréférencement de pointeur NULL conduisant vraisemblablement à un
plantage lors d’un téléversement défectueux avec suivi de la progression.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7063">CVE-2020-7063</a>

<p>Mauvaises permissions de fichiers ajoutés à tar avec Phar::buildFromIterator
lors de leur ré-extraction.</p>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.6.40+dfsg-0+deb8u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2160.data"
# $Id: $
