#use wml::debian::translation-check translation="9e9628d36fe016512982545e03a467362dc4a46a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités dans wordpress, un outil de blog, ont été corrigées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17669">CVE-2019-17669</a>

<p>Une vulnérabilité SSRF (Server Side Request Forgery) est due à une validation
d’URL ne considérant pas l’interprétation d’un nom comme une série de caractères
hexadécimaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17670">CVE-2019-17670</a>

<p>Une vulnérabilité SSRF (Server Side Request Forgery) a été signalée dans
wp_validate_redirect(). Le chemin est à régulariser lors de la validation de
l’emplacement pour les URL relatives.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17671">CVE-2019-17671</a>

<p>La visualisation non authentifiée de certains contenus (billets privés ou
brouillons) est possible parce que la propriété statique de requête est mal
gérée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17675">CVE-2019-17675</a>

<p>Wordpress ne tient pas correctement compte de la confusion de type lors de la
validation du référent dans les pages administratives. Cette vulnérabilité
affecte la fonction check_admin_referer() de WordPress.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.1.28+dfsg-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1980.data"
# $Id: $
