#use wml::debian::translation-check translation="17b358599d12b12e8c91be7109cd2bc67fb15082" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les problèmes suivants ont été découverts dans sdl-image1.2, la version 1.x
de la bibliothèque de chargement de fichier d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3977">CVE-2018-3977</a>

<p>Dépassement de tampon basé sur le tas dans IMG_xcf.c. Cette vulnérabilité
peut être exploitée par des attaquants distants pour provoquer une exécution de
code à distance ou un déni de service à l'aide d'un fichier XCF contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5051">CVE-2019-5051</a>

<p>Dépassement de tampon basé sur le tas dans IMG_LoadPCX_RW, dans IMG_pcx.c.
Cette vulnérabilité peut être exploitée par des attaquants distants pour
provoquer une exécution de code à distance ou un déni de service à l'aide d'un
fichier PCX contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5052">CVE-2019-5052</a>

<p>Dépassement d’entier et dépassement subséquent de tampon dans IMG_pcx.c. Cette
vulnérabilité peut être exploitée par des attaquants distants pour provoquer une
exécution de code à distance ou un déni de service à l'aide d'un fichier PCX
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7635">CVE-2019-7635</a>

<p>Dépassement de tampon basé sur le tas affectant Blit1to4, dans IMG_bmp.c.
Cette vulnérabilité peut être exploitée par des attaquants distants pour
provoquer un déni de service ou d'autres impacts non précisés à l'aide d'un
fichier BMP contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12216">CVE-2019-12216</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12217">CVE-2019-12217</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12218">CVE-2019-12218</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12219">CVE-2019-12219</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12220">CVE-2019-12220</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12221">CVE-2019-12221</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12222">CVE-2019-12222</a>

<p>Plusieurs accès hors limite de lecture et écriture affectant IMG_LoadPCX_RW,
dans IMG_pcx.c. Ces vulnérabilités peuvent être exploitées par des attaquants
distants pour provoquer un déni de service ou tout autre impact non précisé
à l'aide d'un fichier PCX contrefait.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.2.12-5+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets sdl-image1.2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1865.data"
# $Id: $
