#use wml::debian::projectnews::header PUBDATE="2021-03-18" SUMMARY="Bienvenue sur les Nouvelles du projet Debian, élection du chef du projet, gel doux de Bullseye, sécuriser GRUB2, thème Homeworld, bogues critiques, mise à jour du site, APT2.2, QEMU, types MIME, rust-coreutils, debuginfod, Google et Debian, contribution de Freexian, nouvelles clés de signature, chasses aux bogues et contributeurs."
#use wml::debian::acronyms
#use wml::debian::translation-check translation="dc6b8f3e048c8f288c5efd1d612b75bbc4e51406" maintainer="Jean-Pierre Giraud"

# Status: [published]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<shortintro issue="première"/>

<h2>Élections 2021 du responsable du projet Debian</h2>

<p>L'<a href="https://lists.debian.org/debian-devel-announce/2021/03/msg00001.html"> 
appel à candidatures</a> a été publié pour l'élection du <a
href="https://www.debian.org/devel/leader">responsable du projet Debian</a>.
Le processus d'élection débute six semaines avant que le poste ne devienne
vacant, le mandat débutant le 21 avril 2021. Le déroulement du processus
est le suivant : le dépôt des candidatures est ouvert du 7 au 13 mars 2021, la
campagne électorale se déroule du dimanche 14 mars 2021 au 3 avril 2021 et le
vote aura lieu du 4 avril 2021 au 17 avril 2021.</p>


<h2>Gel doux pour Bullseye</h2>

<p>
L'équipe en charge de la publication a annoncé que <a
href="https://lists.debian.org/debian-devel-announce/2021/02/msg00002.html">Bullseye
avait atteint la phase de gel doux le 12 février 2021</a>. Ce gel ne permet
que quelques petites corrections ciblées pour la prochaine publication. Les
nouvelles transitions ou les versions de paquets qui peuvent être perturbatrices
ne sont plus permises. Le programme du gel peut être suivi sur la page <a
href="https://release.debian.org/bullseye/freeze_policy.html">Bullseye Freeze
Timeline and Policy</a>.
</p>

<introtoc/>

<toc-display/>

<toc-add-entry name="security">Annonces de sécurité Debian importantes</toc-add-entry>

<p>L'équipe de sécurité de Debian diffuse au jour le jour les annonces de
sécurité (<a href="$(HOME)/security/2021/">annonces de sécurité pour 2021</a>).
Nous vous conseillons de les lire avec attention et de vous inscrire à la
<a href="https://lists.debian.org/debian-security-announce/">liste de diffusion
correspondante</a> pour garder votre système à jour contre toutes les
vulnérabilités.</p>

<p>Certaines annonces récemment publiées concernent ces paquets :
<a href="$(HOME)/security/2021/dsa-4870">pygments</a>,
<a href="$(HOME)/security/2021/dsa-4869">tiff</a>,
<a href="$(HOME)/security/2021/dsa-4868">flatpack</a>,
<a href="$(HOME)/security/2021/dsa-4867">grub2</a>,
<a href="$(HOME)/security/2021/dsa-4866">thunderbird</a>,
<a href="$(HOME)/security/2021/dsa-4865">docker.io</a>,
<a href="$(HOME)/security/2021/dsa-4862">firefox</a>,
<a href="$(HOME)/security/2021/dsa-4861">screen</a>,
<a href="$(HOME)/security/2021/dsa-4858">chromium</a>,
<a href="$(HOME)/security/2021/dsa-4854">webkit2gtk</a>,
<a href="$(HOME)/security/2021/dsa-4852">openvswitch</a>,
<a href="$(HOME)/security/2021/dsa-4849">firejail</a> et
<a href="$(HOME)/security/2021/dsa-4847">connman</a>.
</p>

<p>Le site web de Debian <a href="https://www.debian.org/lts/security/">archive</a>
maintenant également les annonces de sécurité produites par l'équipe de suivi
à long terme Debian (Debian LTS) et envoyées à la 
<a href="https://lists.debian.org/debian-lts-announce/">liste de diffusion debian-lts-announce</a>.
</p>

<toc-add-entry name="secureboot">Sécuriser UEFI SecureBoot dans GRUB2 — 2021</toc-add-entry>

<p>Depuis la série de bogues <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">BootHole</a>
de GRUB2 annoncés en juillet 2020, les chercheurs en sécurité et les
développeurs de Debian et d'ailleurs ont poursuivi leur recherche de nouveaux
problèmes qui pourraient permettre de contourner l'amorçage sécurisé (Secure
Boot — SB) avec UEFI. Plusieurs autres ont été découverts. Consultez l'<a
href="https://www.debian.org/security/2021/dsa-4867">annonce de sécurité dsa-4867-1</a>
pour plus de détails. Debian a publié une <a
href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot/">déclaration</a>
très enrichissante destinée à expliquer les conséquences de cette vulnérabilité
de sécurité et les étapes pour sa suppression.
</p>

<toc-add-entry name="bullseye">Nouvelles de Debian <q>Bullseye</q></toc-add-entry>

<p><b>Homeworld, le thème par défaut pour <q>Bullseye</q></b></p>
<p>
Nous félicitons Juliette Taka pour sa proposition de
<a href="https://wiki.debian.org/DebianArt/Themes/Homeworld">Homeworld</a>
qui a remporté le concours du thème graphique par défaut pour Debian 11 <q>Bullseye</q>.
Plus de 5 613 votes se sont répartis entre <a 
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">18 contributions</a>.
Nous remercions tous ceux qui ont contribué et voté dans le processus de
sélection qui a été délicieusement rafraîchissant montrant à quel point Debian
est vraiment une communauté.
</p>

<p><b>Rapports de bogue critique pour la semaine 11 de 2021</b></p>

<p>Selon <a href="https://udd.debian.org/bugs.cgi">l'interface web de recherche
de bogues dans la base de données ultime Debian (UDD)</a>, voici les données
actuelles sur les bogues critiques :</p>
<table>
  <tr><th>Total :</th><td><a href="https://udd.debian.org/bugs.cgi?release=any&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1110</a></td></tr>
  <tr><th>Affectant Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">216</a></td></tr>
  <tr><th>Uniquement pour Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_not_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">51</a></td></tr>
  <tr><th>Restant à corriger dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>165</b></a></td></tr>
</table>

<p>Parmi ces <b><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">165</a></b> bogues, sont marqués :</p>

<table>
  <tr><th>En attente dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=only&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Corrigés dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=only&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">23</a></td></tr>
  <tr><th>Dupliqués dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=only&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">10</a></td></tr>
  <tr><th>Pouvant être corrigés dans une mise à jour de sécurité :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=only&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">15</a></td></tr>
  <tr><th>Contrib ou non-free dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=only&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Revendiqués dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=only&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">0</a></td></tr>
  <tr><th>En attente dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=only&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1</a></td></tr>
  <tr><th>Corrigés autrement dans Bullseye :</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=only&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">8</a></td></tr>
</table>

<p>Sans tenir compte de tout ce qu'il y a ci-dessus (plusieurs marquages possibles) <a
href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=ign&amp;pending=ign&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=ign&amp;deferred=ign&amp;notmain=ign&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=ign&amp;done=ign&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>111</b></a>
bogues doivent être corrigés par les contributeurs Debian pour parvenir à la
publication de Debian 11 <q>Bullseye</q>.
</p>

<p>Toutefois, dans la perspective de l'équipe de publication,
<a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=ign&amp;notbullseye=ign&amp;base=&amp;standard=&amp;merged=ign&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>182</b></a>
doivent être traités pour que la publication puisse survenir.</p>

<p>Veuillez consulter la page <q><a
href="https://wiki.debian.org/ProjectNews/RC-Stats">Interpreting the
release critical bug statistics</a></q> pour une explication des différents
chiffres.</p>



<toc-add-entry name="web">Nouvelle présentation du site web</toc-add-entry>

<p>Nous sommes très fiers de notre équipe en charge du site web et de son travail
d'<a href="https://bits.debian.org/2020/12/debian-web-new-homepage.html">actualisation</a>
du <a href="https://www.debian.org">site web de Debian</a> dans un style simplifié et
moderne. Allez y jeter un œil ! C'est juste le début du processus que nous
allons poursuivre par le retrait de la documentation obsolète, le renouvellement
du site avec de nouvelles informations et l'amélioration générale de
l'expérience utilisateur. Comme toujours, mains expertes et yeux critiques
seront utiles, faites nous savoir si vous voulez contribuer à ce nouveau
chapitre de notre développement.</p>

<toc-add-entry name="apt">apt-2.2</toc-add-entry>

<p>Julian Andres Klode <a href="https://blog.jak-linux.org/2021/02/18/apt-2.2/">nous informe</a>
de la publication d'APT 2.2. Les nouvelles fonctionnalités incluent :
<i>--error-on=any</i> et <i>rred</i> comme fonction autonome pour fusionner des
fichiers pdiff.</p>


<toc-add-entry name="keys">Nouvelles clés de signature de l'archive</toc-add-entry>

<p>Les nouvelles clés de signature de l'archive de Debian 11 ont été générées
pour une utilisation dans un proche futur. Les clés seront incluses dans
Debian 11 <q>Bullseye</q> et dans la prochaine version intermédiaire de Debian 10
<q>Buster</q>. Ces clés seront mises en service à la publication de <q>Bullseye</q>
ou à l'expiration des anciennes clés le 12 avril 2027.</p>

Voici les nouvelles clés :
<p>
<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expire le : 15-01-2029]</th></tr>
<tr><th>   </th>   <th>1F89 983E 0081 FDE0 18F3  CC96 73A4 F27B 8DD4 7936</th></tr>
<tr><th>uid</th>   <th>Debian Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th>rsa4096 2021-01-17 [S] [expire le : 15-01-2029]</th></tr>
<tr><th>   </th>   <th>A723 6886 F3CC CAAD 148A  27F8 0E98 404D 386F A1D9</th></tr>
</table>

<p>
<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expire le : 15-01-2029]</th></tr>
<tr><th>   </th>   <th>AC53 0D52 0F2F 3269 F5E9  8313 A484 4904 4AAD 5C5D</th></tr>
<tr><th>uid</th>   <th>Debian Security Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th>rsa4096 2021-01-17 [S] [expire le : 15-01-2029]</th></tr>
<tr><th>   </th>   <th>ED54 1312 A33F 1128 F10B  1C6C 5440 4762 BBB6 E853</th></tr>
</table>

<p>
<b>Clés :</b>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11.asc">https://ftp-master.debian.org/keys/archive-key-11.asc</a></p>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11-security.asc">https://ftp-master.debian.org/keys/archive-key-11-security.asc</a></p>

<toc-add-entry name="dqib">Images préconçues de Debian Quick Image Baker disponibles</toc-add-entry>
<p>DQIB (Debian Quick Image Baker) fournit des
<a href="https://people.debian.org/~gio/dqib/">images de Debian Sid pour QEMU</a>
créées chaque semaine pour plusieurs architectures. Chaque téléchargement
fournit un système de fichiers racine, un noyau, un initrd et un exemple de
fichier README qui comprend la commande de QEMU qui chargera l'image et des
informations sur comment se connecter.

<toc-add-entry name="other">Autres sujets d'intérêt</toc-add-entry> 
<ul>
<li>Charles Plessy <a href="https://lists.debian.org/debian-devel/2021/01/msg00057.html">a
ajouté des centaines de types de média à /etc/mime.types</a>.</li>
<li>Nouvelle liste de diffusion : <a
href="https://lists.debian.org/debian-localgroups/">debian-localgroups@lists.debian.org</a> a
été mise en place pour faciliter la communication et fournir un soutien aux
activités et événements locaux.</li>
</ul>

<toc-add-entry name="rust">Exécution de Debian sur coreutils Rust</toc-add-entry>
<p>Sylvestre Ledru <a href="https://sylvestre.ledru.info/blog/2021/03/09/debian-running-on-rust-coreutils">partage
des détails</a> sur le fonctionnement de <a href="https://tracker.debian.org/pkg/rust-coreutils">rust-coreutils</a>
dans Debian. L'implémentation est capable de prendre en charge l'amorçage avec
GNOME, d'installer les 1000 principaux paquets et de construire Firefox.
</p>

<toc-add-entry name="debuginfod">Nouveau service : debuginfod</toc-add-entry>
<p>Sergio Durigan Junior a annoncé un <a href="https://wiki.debian.org/Debuginfod">service
debuginfod dans Debian</a>. <a 
href="https://sourceware.org/elfutils/Debuginfod.html">debuginfod</a> permet aux
développeurs de se passer du besoin d'installer les paquets debuginfo pour
déboguer les logiciels. Il fonctionne comme un client/serveur pour fournir des
outils de débogage à travers HTTP.
</p>

####Here is where we move to EXTERNAL NEWS about Debian####
## News about Debian, a Debian influence in F/OSS, F/OSS, mentions of Debian 
## in the news or other media.
<toc-add-entry name="bazel">Google et Debian collaborent pour empaqueter le
système de construction Bazel dans Debian</toc-add-entry>

<p>
Le développeur Debian Olek Wojnar et l'informaticien de Google Yun Peng ont
collaboré avec l'<a
href="https://blog.bazel.build/2021/03/04/bazel-debian-packaging.html">équipe de Bazel</a>
pour empaqueter <a href="https://opensourcelive.withgoogle.com/events/bazel/register?after-register
=%2Fevents%2Fbazel%2Fwatch%3Ftalk%3Ddebian-ubuntu-packaging">Bazel dans
Debian</a> pour assister la communauté médicale dans leurs recherches sur la
COVID-19. Olek nous fait partager les défis techniques et des détails sur le
projet dans une
<a href="https://www.youtube.com/watch?v=jLSgky4ISj0&amp;t=23s">vidéo en ligne.</a><p>


<toc-add-entry name="freexian">Freexian va contribuer au financement de certains projets Debian</toc-add-entry>

<p>
Raphaël Hertzog nous communique des détails sur l'intention de LTS de Freexian
de <a href="https://lists.debian.org/debian-project/2020/11/msg00002.html">contribuer
au financement</a> de certains projets Debian avec une partie des fonds
collectés chez ses propres donateurs. Cette contribution généreuse permettra à
des équipes de solliciter un financement dans leurs domaines qui profitera
globalement en retour à l'ensemble de la communauté. 
</p>

<toc-add-entry name="events">Chasses aux bogues, événements, MiniDebCamp et MiniDebConf</toc-add-entry>

<p><b>Chasses aux bogues</b></p>

<p><b>Événements à venir</b></p>

<p>
Il se tiendra une <a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00000.html">chasse
aux bogues virtuelle à Salzbourg, Autriche</a> les 24 et 25 avril 2021. Certains
détails sont encore en phase préparatoire, pour l'instant, retenez la date.</p>

<p><b>Événements passés</b></p>

<p>La communauté Debian du Brésil a tenu la <a href="https://mdcobr2020.debian.net/">MiniDebConf
Online Brasil 2020</a>, les 28 et 29 novembre 2020. Les communications, en
portugais, produites lors de l'événement, sont disponibles pour
<a href="https://peertube.debian.social/videos/watch/playlist/875b2409-2d73-4993-
8471-2923c27b8a7e">visionnage</a>.
</p>

<p>La communauté Debian India a tenu la <a href="https://in2021.mini.debconf.org/">MiniDebConf Online India 2021</a>, 
les 23 et 24 janvier 2021. Les communications sont disponibles en plus de six
langues, avec environ 45 événements au total, partagés entre communications,
rencontres de discussion (BOF) et ateliers. Les communications et des <a 
href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-
India/">vidéos</a>
de l'événement sont disponibles pour ceux qui veulent les voir.
</p>


<toc-add-entry name="reports">Comptes-rendus</toc-add-entry>

<p><b>Comptes-rendus mensuels de Freexian sur Debian Long Term Support (LTS)</b></p>

<p>Freexian publie <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">des comptes-rendus mensuels</a>
sur le travail réalisé par les contributeurs salariés pour le suivi à long terme
de sécurité de Debian.
</p>

<p><b>État d'avancement des compilations reproductibles</b></p>

<p>Suivez le <a
href="https://reproducible-builds.org/blog/">blog des compilations reproductibles</a>
pour obtenir un compte-rendu de leur travail sur le cycle de <q>Buster</q>.
</p>


<p><b>Paquets qui ont besoin de travail</b></p>

<wnpp link="https://lists.debian.org/debian-devel/2021/03/msg00063.html"
        orphaned="1204"
        rfa="209" />

<p><b>Bogues pour débutants</b></p>

<p>
Debian utilise l'étiquette <q>newcomer</q> (débutant) pour signaler les
bogues qui sont adaptés aux nouveaux contributeurs comme point d'entrée pour
travailler sur des paquets particuliers.

Il y a <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">189</a>
bogues marqués <q>newcomer</q> disponibles.
</p>

<toc-add-entry name="code">Code, développeurs et contributeurs</toc-add-entry>
<p><b>Nouveaux responsables de paquets depuis le 9 septembre 2020</b></p>

<p>
Bienvenue à Adrien Nayrat, Georgy Komarov, Alex Doyle, Johann Queuniet,
Stephen Gildea, Christoph Groth, Jhon Alejandro Marin Rodriguez,
Adrià García-Alzórriz, Romain Porte, Jakub Ružička, skyper, James Turton, Alois
Schlögl, Judit Foglszinger, Aaron Boxer, Kevin Wu, Anthony Perkins, Felix
Delattre, Ken Ibbotson, Andrei Rozanski, Nis Martensen , qinxialei, Laurin
Hagemann, Jai Flack, Johann Elsass, Fred Le Meur, Vivek K J, Thiago da Silva
Gracini, Jobin J, Selvamani Kannan, Calum McConnell, Dhyey Patel, Ed Neville,
Leonidas S. Barbosa, Lucca Braga Godoy Mendonça, Chris Keller, Guinness,
Sergio de Almeida Cipriano Junior, Sahil Dhiman, Michel Le Bihan, Fabio
Fantoni, Mark Pearson, Matija Nalis, David Bannon, Federico Grau, Lisa Julia
Nebel, Patrick Jaap, Francisco Emanoel Ferreira, Peymaneh Nejad, Daniel Milde,
Stefan Kropp, Frédéric Pierret, Vipul Kumar, Jarrah Gosbell, John Zaitseff,
Badreddin Aboubakr, Sam Reed, Scupake, Clay Stan, Klaumi Klingsporn, Vincent
Smeets, Emerson dos Santos Queiroz, Alexander Sulfrian, bill-auger, Marcelo
Henrique Cerri, Dan Streetman, Hu Feng, Andrea Righi, Matthias Klein, Eric
Brown, Mayco Souza Berghetti, Robbi Nespu, Simon Tatham et Brian Potkin.
</p>

<p><b>Nouveaux responsables Debian</b></p>

<p>
Bienvenue à Ricardo Ribalda Delgado, Pierre Gruet, Henry-Nicolas Tourneur,
Aloïs Micard, Jérôme Lebleu, Nis Martensen, Stephan Lachnit, Felix Salfelder,
Aleksey Kravchenko, Étienne Mollier, Timo Röhling, Fabio Augusto De Muzio
Tobich, Arun Kumar Pariyar, Francis Murtagh, William Desportes, Robin
Gustafsson, Nicholas Guriev, Xiang Gao, Maarten L. Hekkelman, qinxialei,
Boian Bonev, Filip Hroch et Antonio Valentino.
</p>

<p><b>Nouveaux développeurs Debian</b></p>
<p>
Bienvenue à Benda XU, Joseph Nahmias, Marcos Fouces, Hayashi Kentaro,
James Valleroy, Helge Deller, Nicholas D Steeves, Nilesh Patra, David Suárez
Rodríguez et Pierre Gruet.
</p>

<p><b>Contributeurs</b></p>

<p>
954 personnes et 9 équipes sont actuellement recensées dans la page des
<a href="https://contributors.debian.org/">contributeurs</a> Debian pour 2021.
</p>

<p><b>Statistiques</b></p>

<p><b><em>Buster</em></b></p>
<ul style="list-style-type:none">
<li>Fichiers source : 12 323 884</li>
<li>Paquets source : 28 925</li>
<li>Espace disque utilisé : 264 071 084 ko</li>
<li>Ctags : 9 487 034</li>
<li>Lignes de code source : 1 077 110 982</li>
</ul>

<p><b><em>Sid</em></b></p>
<ul style="list-style-type:none">
<li>Fichiers source : 16 868 320</li>
<li>Paquets source : 33 215</li>
<li>Espace disque utilisé : 364 735 804 ko</li>
<li>Ctags : 3 343 666</li>
<li>Lignes de code source : 1 510 195 519</li>
</ul>

<p><b>Paquets populaires</b></p>
##Taken via UDD query from popcon
<ul>
<li><a href="https://packages.debian.org/buster/tar">tar</a> : 99 100 utilisateurs quotidiens</li>
<li><a href="https://packages.debian.org/buster/popularity-contest">popularity-contest</a> : 96 010 utilisateurs quotidiens</li>
<li><a href="https://packages.debian.org/buster/debianutils">debianutils</a> : 112 659 installations</li>
<li><a href="https://packages.debian.org/buster/gparted">gparted</a> : 14 909 installations</li>
<li><a href="https://packages.debian.org/buster/grub2-common">grub2-common</a> : 54 040 mises à niveau récentes</li>
</ul>

<p><b>Nouveaux paquets ou paquets dignes d'intérêt</b></p>

<p>
Voici quelques-uns des nombreux paquets <a href="https://packages.debian.org/unstable/main/newpkg">
introduits dans l'archive de Debian unstable</a> ces dernières semaines :</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/dnsperf">dnsperf – outil de test de performance de DNS</a></li>
<li><a href="https://packages.debian.org/unstable/main/ptpython">ptpython – interpréteur Python alternatif avec complétion automatique</a></li>
<li><a href="https://packages.debian.org/unstable/main/science-datamanagement">science-datamanagement – paquets de gestion de données de Debian Science</a></li>
<li><a href="https://packages.debian.org/unstable/main/dnscap">dnscap – utilitaire de capture de réseau conçu spécialement pour le trafic DNS</a></li>
<li><a href="https://packages.debian.org/unstable/main/logiops">logiops – utilitaire de configuration pour les souris et claviers Logitech</a></li>
<li><a href="https://packages.debian.org/unstable/main/node-cron-validator">node-cron-validator – cron-validator est un outil de validation d'expression cron</a></li>
</ul>

<toc-add-entry name="discuss">Discussions</toc-add-entry>

<p>Le développeur Debian Stephan Lachnit pose la question : <a href="https://lists.debian.org/debian-devel/2021/02/msg00282.html">
<i>est-il possible de contribuer à Debian sans utiliser son nom réel par souci
de protection de la vie privée</i></a> ?
</p>

<p>William Torrez Corea s'interroge : <a href="https://lists.debian.org/debian-user/2021/01/msg00321.html"><i>comment
mettre à niveau du système d'exploitation vers Debian Buster</i></a>? Le fil de
discussion s’intéresse au choix entre la réinstallation, la mise à niveau ou la
reconstruction, et présente une synthèse sur Sid, Stable et Testing.
</p>

<p>Jerry Mellon pose la question : <a  href="https://lists.debian.org/debian-user/2021/01/msg00584.html"><i>comment
ajouter un disque dur à un système déjà en place</i></a> ? Lecture aisée qui
expose certains écueils et solutions pour cette tâche très courante.
</p>

<p>Dan Hitt s'interroge :  <a href="https://lists.debian.org/debian-user/2021/01/msg00746.html"><i>comment
installer Debian 10 sans accès à un CD ou à l'USB mais avec l'utilisation
d’Ethernet et du disque dur</i></a> ? La discussion a porté sur les BIOS
ultra-rapides, les options de pxeboot, les entrées de menu de grub, l'amorçage
par le réseau et la solution du noyau sur un disque dur.
</p>

<p>John Berden se questionne : <a href="https://lists.debian.org/debian-user/2021/02/msg00164.html"><i>comment
corriger un mot de passe incorrect dans Debian 10.8 après l'installation</i></a> ?.
Cette discussion traite de l'édition de grub au démarrage, la persistance de
grub, une courte leçon sur la syntaxe d'emacs et de l'éditeur de grub, ainsi
que du comportement canonique d'interpréteur de commande.
</p>

<p><b>Trucs et astuces</b></p>

<ul>
<li>Craig Small explique les <a href="https://dropbear.xyz/2021/01/18/percent-cpu-for-processes/">champs
CPU/pcpu du programme ps</a>, que la plupart d'entre nous exécutent avec la
commande ps aux. Il présente une longue explication sur la façon dont les
calculs sont collationnés et présentés.</li>

<li>Vincent Fourmond partage tous ses <a href="https://vince-debian.blogspot.com/2021/03/all-tips-and-tricks-about-qsoas.
html">trucs et astuces sur QSoas</a>.</li>

<li>Bastian Venthur détaille l'<a href="https://venthur.de/2021-02-10-installing-debian-on-a-thinkpad-t14s.
html">installation de Debian sur un Thinkpad T14s</a>.</li>
</ul>

<p><b>Il était une fois dans Debian :</b></p>

<ul>
<li>du 13 au 19 mars 2010 <a href="https://wiki.debian.org/DebianThailand/MiniDebCamp2010">MiniDebCamp 2010 en Thaïlande</a></li>

<li>du 15 au 16 mars 2014 <a href="https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/">Mini-DebConf Femmes à Barcelone, Espagne</a></li>

<li>le 16 mars 2000 <a href="https://www.debian.org/vote/2000/vote_0007">Wichert Akkerman réélu responsable du projet Debian</a></li>

<li>le 17 mars 1997 <a href="https://www.debian.org/News/1997/19970317">Ian Murdock élu président du conseil d'administration</a></li>

<li>le 17 mars 2005 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=300000">le bogue Debian n° 300 000 rapporté par Florian Zumbiehl</a></li>

<li>le 17 mars 2008 <a href="https://www.debian.org/devel/debian-installer/News/2008/20080317">publication de la première version bêta de l'installateur Debian pour Lenny</a></li>
</ul>

<toc-add-entry name="continuedpn">Continuer à lire les Nouvelles du projet Debian</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">S'inscrire ou se désinscrire </a> de la liste de diffusion Debian News</p>

#use wml::debian::projectnews::footer editor="l'équipe en charge de la publicité avec des contributions de Jean-Pierre Giraud, Justin B Rye, Thiago Pezzo, Paulo Santana, Donald Norwood" translator="Jean-Pierre Giraud, l\'équipe francophone de traduction"
