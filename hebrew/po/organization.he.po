msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2008-06-17 13:57+0200\n"
"Last-Translator: Oz Nahum <nahumoz@gmail.com>\n"
"Language-Team: Hebrew <he@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "חבר"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "מנהל"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "Stable Release Manager"
msgstr "הנהלת השחרור"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "אשף"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "יושב ראש"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "עוזר"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "מזכיר"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "קצינים"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "הפצה"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
#, fuzzy
msgid "Publicity team"
msgstr "יחסי ציבור"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "תמיכה ותשתית"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "מנהיג"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "וועדה טכנית"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "מזכיר"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "מיזמי פיתוח"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "ארכיוני FTP"

#: ../../english/intro/organization.data:114
#, fuzzy
msgid "FTP Masters"
msgstr "מנהל ה-FTP"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "סייעי FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "הנהלת השחרור"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "צוות השחרור"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "אבטחת איכות"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "צוות התקנת המערכת"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "הודעות שחרור"

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr ""

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "הפקה"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "בחינה"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "תמיכה ותשתית"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
#, fuzzy
msgid "Buildd administration"
msgstr "ניהול מערכת"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "תיעוד"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "עבודה נדרשת והצעות לאריזת תוכנה"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "איש קשר לעיתונות"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "דפי רשת"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "פלאנט דביאן"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "אירועים"

#: ../../english/intro/organization.data:264
#, fuzzy
msgid "DebConf Committee"
msgstr "וועדה טכנית"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "שותפים לתוכנית"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "מערכת מעקב באגים"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "ניהול רשימות תפוצה וארכיון רשימות תפוצה"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "מנהלי חשבונות דביאן"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "צוות אבטחה"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "מדיניות"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "ניהול מערכת"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr ""

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr ""

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "ניהול מערכת"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "צוות התקנת המערכת"

#~ msgid "Publicity"
#~ msgstr "יחסי ציבור"

#~ msgid "Custom Debian Distributions"
#~ msgstr "הפצות דביאן מותאמות אישית"

#~ msgid "Release Team for ``stable''"
#~ msgstr "צוות השחרור להפצה ה'יציבה'"

#~ msgid "Vendors"
#~ msgstr "ספקים"

#~ msgid "APT Team"
#~ msgstr "צוות APT"

#~ msgid "Handhelds"
#~ msgstr "מחשבי כף יד"

#~ msgid "Marketing Team"
#~ msgstr "צוות שיווק"

#~ msgid "Testing Security Team"
#~ msgstr "צוות אבטחה להפצת הבחינה"

#~ msgid "Individual Packages"
#~ msgstr "חבילות עצמאיות"

#~ msgid "User support"
#~ msgstr "תמיכת משתמשים"

#~ msgid "Firewalls"
#~ msgstr "חומות אש"

#~ msgid "Laptops"
#~ msgstr "מחשבים ניידים"

#~ msgid "Special Configurations"
#~ msgstr "הגדרות מיוחדות"
