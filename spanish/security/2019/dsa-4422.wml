#use wml::debian::translation-check translation="9bf43d9ebe1fe8f0f648cd5c0431b530d1105d92"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado varias vulnerabilidades en el servidor HTTP Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17189">CVE-2018-17189</a>

    <p>Gal Goldshtein, de F5 Networks, descubrió una vulnerabilidad de denegación
    de servicio en mod_http2. Mediante el envío de solicitudes mal construidas, el
    flujo de datos («stream») http/2 para esa solicitud ocupaba, innecesariamente, un hilo del
    servidor, borrando los datos entrantes y dando lugar a denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17199">CVE-2018-17199</a>

    <p>Diego Angulo, de ImExHS, descubrió que mod_session_cookie no
    respetaba el tiempo de expiración.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0196">CVE-2019-0196</a>

    <p>Craig Young descubrió que era posible hacer que la gestión de solicitudes http/2
    en mod_http2 accediera a memoria liberada al comparar cadenas durante la
    determinación del método de una solicitud y que, en consecuencia, procesara incorrectamente
    la solicitud.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0211">CVE-2019-0211</a>

    <p>Charles Fol descubrió una elevación de privilegios desde el
    proceso hijo (con menos privilegios) al proceso padre (que se ejecuta como root).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

    <p>Una condición de carrera en mod_auth_digest cuando se ejecuta en un servidor
    multihilo podría permitir que un usuario con credenciales válidas se autenticara
    con otro nombre de usuario, sorteando las restricciones de control de acceso
    configuradas. Simon Kappel descubrió este problema.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

    <p>Bernhard Lorenz, de Alpha Strike Labs GmbH, informó de que las normalizaciones
    de URL se gestionaban de forma inconsistente. Cuando el componente de ruta
    de la URL de una solicitud contiene múltiples barras ('/') consecutivas,
    directivas como LocationMatch y RewriteRule deben tener en cuenta las barras
    duplicadas en las expresiones regulares, mientras que otros aspectos del procesamiento
    del servidor eliminan las duplicadas de forma implícita.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 2.4.25-3+deb9u7.</p>

<p>Esta actualización contiene también correcciones de fallos que estaban programadas para su inclusión en la
próxima versión de la distribución «estable». Esto incluye la corrección de una regresión introducida por una
corrección de seguridad en la versión 2.4.25-3+deb9u6.</p>

<p>Le recomendamos que actualice los paquetes de apache2.</p>

<p>Para información detallada sobre el estado de seguridad de apache2, consulte su página
en el sistema de seguimiento de problemas de seguridad: <a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4422.data"
