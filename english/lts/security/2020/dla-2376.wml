<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the Qt toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19872">CVE-2018-19872</a>

    <P>A malformed PPM image causes a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17507">CVE-2020-17507</a>

    <p>Buffer over-read in the XBM parser.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
5.7.1+dfsg-3+deb9u3.</p>

<p>We recommend that you upgrade your qtbase-opensource-src packages.</p>

<p>For the detailed security status of qtbase-opensource-src please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qtbase-opensource-src">https://security-tracker.debian.org/tracker/qtbase-opensource-src</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2376.data"
# $Id: $
