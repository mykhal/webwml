<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple buffer overflow security issues have been found in libsdl2,
a library that allows low level access to a video frame buffer, audio
output, mouse, and keyboard.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.0.2+dfsg1-6+deb8u1.</p>

<p>We recommend that you upgrade your libsdl2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1714.data"
# $Id: $
