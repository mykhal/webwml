<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an arbitrary code execution vulnerability
in the pump DHCP/BOOTP client.</p>

<p>When copying the body of the server response, the ethernet packet length
could be forged leading to being able to overwrite stack memory. Thanks to
&lt;ltspro2@secmail.pro&gt; for the report and patch.
(<a href="https://bugs.debian.org/933674">#933674</a>)</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.8.24-7+deb8u1.</p>

<p>We recommend that you upgrade your pump packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1908.data"
# $Id: $
