<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple researchers have discovered a vulnerability in the way the
Intel processor designs have implemented speculative execution of
instructions in combination with handling of page-faults. This flaw
could allow an attacker controlling an unprivileged process to read
memory from arbitrary (non-user controlled) addresses, including from
the kernel and all other processes running on the system or cross
guest/host boundaries to read host memory.</p>

<p>To fully resolve these vulnerabilities it is also necessary to install
updated CPU microcode (only available in Debian non-free). Common server
class CPUs are covered in the update released as DLA 1446-1.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.110-3+deb9u4~deb8u1.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1481.data"
# $Id: $
