<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the SocketServer class included in
apache-log4j1.2, a logging library for java, is vulnerable to
deserialization of untrusted data. An attacker can take advantage of
this flaw to execute arbitrary code in the context of the logger
application by sending a specially crafted log event.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1.2.17-7+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1.2.17-8+deb10u1.</p>

<p>We recommend that you upgrade your apache-log4j1.2 packages.</p>

<p>For the detailed security status of apache-log4j1.2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache-log4j1.2">\
https://security-tracker.debian.org/tracker/apache-log4j1.2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4686.data"
# $Id: $
