#use wml::debian::template title="人们：我们是谁，我们做什么"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ac8343f61b83fe988dbcb035d77af5bf09ba0709"

# translators: some text is taken from /intro/about.wml

<h2>开发者与贡献者</h2>
<p>Debian 是由分布在<a href="$(DEVEL)/developers.loc">世界各地</a>的近千名
活跃开发人员在业余时间自愿开发的。
很少有开发者真的见过面。
通信主要通过电子邮件（邮件列表参见 lists.debian.org）
和 IRC（irc.debian.org 的 #debian 频道）进行。
</p>

<p>官方 Debian 成员的完整列表可在 
<a href="https://nm.debian.org/members">nm.debian.org</a>找到，同时也在此处
管理成员资格。更广泛的 Debian 贡献者列表请见 
<a href="https://contributors.debian.org">contributors.debian.org</a>。</p>

<p>Debian 项目拥有一个精心组织的<a href="organization">组织架构</a>。
有关 Debian 内部的更多信息，请自由浏览
<a href="$(DEVEL)/">开发者之家</a>。</p>

<h3><a name="history">一切从何开始？</a></h3>

<p>Debian 于1993年8月由 Ian Murdock 创建，作为一个新的发行版，它遵循 Linux 和
 GNU 的精神公开发布。Debian 的本意是要将软件小心而认真地组合在一起，并以
类似的方式进行系统维护和支持。它最初是一个由自由软件极客组成的小团体，后
来逐渐发展成为一个由开发人员和用户组成的组织良好的大型社区。查看
<a href="$(DOC)/manuals/project-history/">详细历史记录</a>。

<p>由于很多人都问过（这里解答一下），Debian的发音是 /&#712;de.bi.&#601;n/。它
来自于 Debian 的创造者 Ian Murdock 和他的妻子 Debra 的名字。
  
<h2>支持 Debian 的个人和组织</h2>

<p>许多其他个人和组织都是 Debian 社区的一部分：
<ul>
  <li><a href="https://db.debian.org/machines.cgi">主机和硬件资助者</a></li>
  <li><a href="../mirror/sponsors">镜像站资助者</a></li>
  <li><a href="../partners/">发展和服务伙伴</a></li>
  <li><a href="../consultants">顾问</a></li>
  <li><a href="../CD/vendors">Debian 安装介质供应商</a></li>
  <li><a href="../distrib/pre-installed">预装 Debian 的计算机供应商</a></li>
  <li><a href="../events/merchandise">相关商品销售商</a></li>
</ul>

<h2><a name="users">谁使用 Debian？</a></h2>

<p>尽管没有精确的统计数据（因为 Debian 不要求用户注册），但有证据表明 
Debian 被各种各样的大小组织，以及成千上万的个人所使用。
看看<a href="../users/">谁在用 Debian？</a>页面列出了一些知名组织，
这些组织提交了关于如何以及为什么使用 Debian 的简短描述。
