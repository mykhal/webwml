#use wml::debian::template title="Αποκτώντας το Debian"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="50283624a3deec87adfe5a87644c4aad93d91c53" maintainer="galaxico"

<p>Το Debian διανέμεται <a href="../intro/free">ελεύθερα</a>
στο Διαδίκτυο. Μπορείτε να το μεταφορτώσετε ολόκληρο από οποιονδήποτε από τους 
<a href="ftplist">καθρέφτες</a> μας.
Το <a href="../releases/stable/installmanual">Εγχειρίδιο 
Εγκατάστασης</a> περιέχει λεπτομερείες οδηγίες για την εγκατάσταση.
Και οι σημειώσεις της έκδοσης μπορούν να βρεθούν <a 
href="../releases/stable/releasenotes">εδώ</a>.
</p>

<p>Αν θέλετε απλά να εγκαταστήσετε το Debian, αυτές είναι οι επιλογές σας:</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Μεταφορτώστε μια εικόνα για την εγκατάσταση</a></h2>
    <p>Ανάλογα με την σύνδεσή σας στο Διαδίκτυο, μπορείτε να μεταφορτώσετε ένα 
από τα ακόλουθα:</p>
    <ul>
      <li>Μια <a href="netinst"><strong>μικρή εικόνα εγκατάστασης</strong></a>:
	    μπορεί να μεταφορτωθεί γρήγορα και θα πρέπει να εγγραφεί σε έναν 
αφαιρέσιμο δίσκο. Για να την χρησιμοποιήσετε, θα χρειαστείτε μια σύνδεση στο 
Διαδίκτυο.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	</ul>
      </li>
      <li>Μια μεγαλύτερη <a href="../CD/"><strong>πλήρη εικόνα 
εγκατάστασης </strong></a>: περιέχει περισσότερα πακέτα, κάνοντας ευκολότερη 
την εγκατάσταση σε μηχανήματα χωρίς σύνδεση στο Διαδίκτυο.
	<ul class="quicklist downlist">
	  <li><a title="Download DVD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Download DVD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	  <li><a title="Download CD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC torrents (CD)</a></li>
	  <li><a title="Download CD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC torrents (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Χρησιμοποιήστε μια 
εικόνα Debian στο νέφος</a></h2>
    <ul>
      <li>Μια επίσημη <a 
href="https://cloud.debian.org/images/cloud/"><strong>εικόνα 
στο νέφος (cloud)</strong></a>:
            μπορεί να χρησιμοποιηθεί απευθείας στον πάροχο του νέφους, 
μεταγλωττισμένη από την ομάδα Νέφους του Debian (Debian Cloud Team).
        <ul class="quicklist downlist">
          <li><a title="OpenStack image for 64-bit Intel and AMD Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">64-bit AMD/Intel OpenStack (Qcow2)</a></li>
          <li><a title="OpenStack image for 64-bit ARM Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">64-bit ARM OpenStack (Qcow2)</a></li>
        </ul>
      </li>
    </ul>
    <h2><a href="../CD/live/">Δοκικμάστε το Debian live πριν 
εγκαταστήσετε</a></h2>
    <p>
    Μπορείτε να δοκιμάσετε το Debian εκκινώντας ένα "ζωντανό" (live) σύστημα 
    από ένα CD, DVD ή κλειδί USB χωρίς να εγκαταστήσετε οποιαδήποτε αρχεία 
    στον υπολογιστή σας. Όταν είστε έτοιμοι/ες μπορείτε να εκτελέσετε τον 
    εγκαταστάτη που συμπεριλαμβάνεται. Εφόσον το μέγεθος των διαθέσιμων 
    εικόνων εγκατάστασης, η γλώσσα και η επιλογή πακέτων ικανοποιούν τις 
    απαιτήσεις σας αυτή η μέθοδος ίσως είναι κατάλληλη για σας.
      Διαβάζοντας περισσότερα στη σελίδα <a 
href="../CD/live#choose_live">πληροφορίες σχετικά με αυτή τη μέθοδο</a>
      θα σας βοηθήσει να αποφασίσετε.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-bit PC live 
torrent</a></li>
      <li><a title="Download live torrents for normal 32-bit Intel and AMD PC"
	     href="<live-images-url/>/i386/bt-hybrid/">32-bit PC live 
torrent</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Αγοράστε ένα σετ CD ή DVD από έναν από τους 
προμηθευτές που πουλάνε CD του Debian</a></h2>

   <p>
      Αρκετοί προμηθευτές πουλάνε τη διανομή για λιγότερα από 5 δολάρια συν το 
κόστος της αποστολής (ελέγξτε την ιστοσελίδα τους για να δείτε αν κάνουν 
διεθνείς αποστολές).
      <br />
      Μερικά από τα <a href="../doc/books">βιβλία σχετικά με το 
Debian</a> έρχονται επίσης με CD.
   </p>

   <p>Αυτά είναι τα βασικά πλεονεκτήματα των CD:</p>

   <ul>
     <li>Η εγκατάσταση από ένα CD είναι πιο άμεση.</li>
     <li>Μπορείτε να εγκασταστήσετε σε μηχανήματα χωρίς σύνδεση στο 
Διαδίκτυο.</li>
	 <li>Μπορείτε να εγκαταστήσετε το Debian (σε όσα μηχανήματα θέλετε) 
χωρίς να μεταφορτώσετε οι ίδιοι/ες όλα τα πακέτα.</li>
     <li>Το CD μπορεί να χρησιμοποιηθεί πιο εύκολα για να διασώσετε ένα 
σύστημα Debian που έχει υποστεί κάποια ζημιά.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Αγοράστε έναν υπολογιστή με προεγκατεστημένο το 
Debian
       </a></h2>
   <p>Αυτό έχει έναν αριθμό πλεονεκτημάτων:</p>
   <ul>
    <li>Δεν χρειάζεται να εγκαταστήσετε εσείς το Debian.</li>
    <li>Η εγκατάσταση έχει από πριν ρυθμιστεί έτσι ώστε να ταιριάζει στο υλικό 
του υπολογιστή.</li>
    <li>Ο προμηθευτής ενδέχεται να προσφέρει τεχνική υποστήριξη.</li>
   </ul>
  </div>
</div>
